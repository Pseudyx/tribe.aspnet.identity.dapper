﻿namespace Tribal.AspNet.Identity.Dapper
{
    //
    // Summary:
    //     EntityType that represents one specific user claim
    //
    // Type parameters:
    //   TKey:
    public class IdentityUserClaim<TKey>
    {
        public IdentityUserClaim() { }

        //
        // Summary:
        //     Claim type
        public virtual string ClaimType { get; set; }
        //
        // Summary:
        //     Claim value
        public virtual string ClaimValue { get; set; }
        //
        // Summary:
        //     Primary key
        public virtual int Id { get; set; }
        //
        // Summary:
        //     User Id for the user who owns this login
        public virtual TKey UserId { get; set; }
    }
}
