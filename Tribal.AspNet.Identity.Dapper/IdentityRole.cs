﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;

namespace Tribal.AspNet.Identity.Dapper
{
    /// <summary>
    ///     Represents a Role entity
    /// </summary>
    /// <typeparam name="TKey">TKey</typeparam>
    public class IdentityRole<TKey, TUserRole> : IRole<TKey>
        where TUserRole : IdentityUserRole<TKey>
    {
        /// <summary>
        ///     Constructor
        /// </summary>
        public IdentityRole()
        {
        }

        /// <summary>
        ///     Constructor with name param
        /// </summary>
        public IdentityRole(string name) : this()
        {
            Name = name;
        }

        /// <summary>
        ///     Constructor with name and id params
        /// </summary>
        public IdentityRole(string name, TKey id)
        {
            Name = name;
            Id = id;
        }

        /// <summary>
        ///     Role id
        /// </summary>
        public TKey Id { get; set; }

        /// <summary>
        ///     Role Name
        /// </summary>
        public string Name { get; set; }

        //
        // Summary:
        //     Navigation property for users in the role
        public virtual ICollection<TUserRole> Users { get; }
    }
}
