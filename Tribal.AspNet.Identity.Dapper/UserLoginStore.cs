﻿using Microsoft.AspNet.Identity;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Tribal.AspNet.Identity.Dapper
{
    public class UserLoginStore<TKey, TUserLogin, TUserRole, TUserClaim> : DbContext
        where TUserLogin : IdentityUserLogin<TKey>
        where TUserRole : IdentityUserRole<TKey>
        where TUserClaim : IdentityUserClaim<TKey>
    {

        /// <summary>
        /// Constructor that takes a DbManager instance 
        /// </summary>
        /// <param name="database"></param>
        public UserLoginStore(string connectionString) : base(connectionString)
        {
        }

        /// <summary>
        /// Deletes a login from a user in the UserLogins table
        /// </summary>
        /// <param name="user">User to have login deleted</param>
        /// <param name="login">Login to be deleted from user</param>
        /// <returns></returns>
        public void Delete(IdentityUser<TKey, TUserLogin, TUserRole, TUserClaim> user, UserLoginInfo login)
        {
            Connection.Execute(@"Delete from MemberLogin 
                    where UserId = @userId 
                    and LoginProvider = @loginProvider 
                    and ProviderKey = @providerKey",
                new
                {
                    userId = user.Id,
                    loginProvider = login.LoginProvider,
                    providerKey = login.ProviderKey
                });
        }

        /// <summary>
        /// Deletes all Logins from a user in the UserLogins table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public void Delete(TKey userId)
        {
            Connection.Execute(@"Delete from MemberLogin 
                    where UserId = @userId", new { userId = userId });
        }

        /// <summary>
        /// Inserts a new login in the UserLogins table
        /// </summary>
        /// <param name="user">User to have new login added</param>
        /// <param name="login">Login to be added</param>
        /// <returns></returns>
        public void Insert(IdentityUser<TKey, TUserLogin, TUserRole, TUserClaim> user, UserLoginInfo login)
        {
            Connection.Execute(@"Insert into MemberLogin 
                (LoginProvider, ProviderKey, UserId) 
                values (@loginProvider, @providerKey, @userId)",
                    new
                    {
                        loginProvider = login.LoginProvider,
                        providerKey = login.ProviderKey,
                        userId = user.Id
                    });
        }

        /// <summary>
        /// Return a userId given a user's login
        /// </summary>
        /// <param name="MemberLogin">The user's login info</param>
        /// <returns></returns>
        public TKey FindUserIdByLogin(UserLoginInfo MemberLogin)
        {
            return Connection.ExecuteScalar<TKey>(@"Select UserId from MemberLogin 
                where LoginProvider = @loginProvider and ProviderKey = @providerKey",
                        new
                        {
                            loginProvider = MemberLogin.LoginProvider,
                            providerKey = MemberLogin.ProviderKey
                        });
        }

        /// <summary>
        /// Returns a list of user's logins
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public List<UserLoginInfo> FindByUserId(TKey userId)
        {
            return Connection.Query<UserLoginInfo>("Select * from MemberLogin where MemberId = @userId", new { userId = userId })
                .ToList();
        }
    }
}
