﻿namespace Tribal.AspNet.Identity.Dapper
{
    //
    // Summary:
    //     EntityType that represents a user belonging to a role
    //
    // Type parameters:
    //   TKey:
    public class IdentityUserRole<TKey>
    {
        public IdentityUserRole() { }

        //
        // Summary:
        //     RoleId for the role
        public virtual TKey RoleId { get; set; }
        //
        // Summary:
        //     UserId for the user that is in the role
        public virtual TKey UserId { get; set; }
    }
}
