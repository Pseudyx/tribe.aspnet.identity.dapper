﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Claims;
using System.Linq;

namespace Tribal.AspNet.Identity.Dapper
{
    public class UserClaimStore<TKey, TUserLogin, TUserRole, TUserClaim> : DbContext
        where TUserLogin : IdentityUserLogin<TKey>
        where TUserRole : IdentityUserRole<TKey>
        where TUserClaim : IdentityUserClaim<TKey>
    {
        /// <summary>
        /// Constructor that takes a DbManager instance 
        /// </summary>
        /// <param name="database"></param>
        public UserClaimStore(string connectionString) : base(connectionString)
        {
        }

        /// <summary>
        /// Returns a ClaimsIdentity instance given a userId
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public ClaimsIdentity FindByUserId(TKey userId)
        {
            ClaimsIdentity claims = new ClaimsIdentity();

            foreach (var c in Connection.Query("Select * from MemberClaim where MemberId=@userId", new { userId = userId }))
            {
                claims.AddClaim(new Claim(c.ClaimType, c.ClaimValue));
            }

            return claims;
        }

        /// <summary>
        /// Deletes all claims from a user given a userId
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public void Delete(TKey userId)
        {
            Connection.Execute(@"Delete from MemberClaim where UserId = @userId", new { userId = userId });
        }

        /// <summary>
        /// Inserts a new claim in UserClaims table
        /// </summary>
        /// <param name="MemberClaim">User's claim to be added</param>
        /// <param name="userId">User's id</param>
        /// <returns></returns>
        public void Insert(Claim MemberClaim, TKey userId)
        {
            Connection.Execute(@"Insert into MemberClaim (ClaimValue, ClaimType, UserId) 
                values (@value, @type, @userId)",
                    new
                    {
                        value = MemberClaim.Value,
                        type = MemberClaim.Type,
                        userId = userId
                    });
        }

        /// <summary>
        /// Deletes a claim from a user 
        /// </summary>
        /// <param name="user">The user to have a claim deleted</param>
        /// <param name="claim">A claim to be deleted from user</param>
        /// <returns></returns>
        public void Delete(IdentityUser<TKey,TUserLogin, TUserRole, TUserClaim> user, Claim claim)
        {
            Connection.Execute(@"Delete from MemberClaim 
            where UserId = @userId and @ClaimValue = @value and ClaimType = @type",
                new
                {
                    userId = user.Id,
                    ClaimValue = claim.Value,
                    type = claim.Type
                });
        }
    }
}
