﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Tribal.AspNet.Identity.Dapper
{
    public class UserRoleStore<TKey, TUserLogin, TUserRole, TUserClaim> : DbContext
        where TUserLogin : IdentityUserLogin<TKey>
        where TUserRole : IdentityUserRole<TKey>
        where TUserClaim : IdentityUserClaim<TKey>
    {
        /// <summary>
        /// Constructor that takes a DbManager instance 
        /// </summary>
        /// <param name="database"></param>
        public UserRoleStore(string connectionString) : base(connectionString)
        {}

        /// <summary>
        /// Returns a list of user's roles
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public List<string> FindByUserId(TKey userId)
        {
            return Connection.Query<string>("Select Role.Name from MemberRole, Role where MemberRole.MemberId=@userId and MemberRole.RoleId = Role.Id", new { userId = userId })
                .ToList();
        }

        /// <summary>
        /// Deletes all roles from a user in the UserRoles table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public void Delete(TKey userId)
        {
            Connection.Execute(@"Delete from MemberRole where Id = @userId", new { userId = userId });
        }

        /// <summary>
        /// Inserts a new role for a user in the UserRoles table
        /// </summary>
        /// <param name="user">The User</param>
        /// <param name="roleId">The Role's id</param>
        /// <returns></returns>
        public void Insert(IdentityUser<TKey, TUserLogin, TUserRole, TUserClaim> user, TKey roleId)
        {
            Connection.Execute(@"Insert into AspNetUserRoles (UserId, RoleId) values (@userId, @roleId",
                new { userId = user.Id, roleId = roleId });
        }
    }
}
