﻿using Dapper;

namespace Tribal.AspNet.Identity.Dapper
{
    /// <summary>
    /// Class that represents the Role table in the Database
    /// </summary>
    public class RoleDbContext<TKey, TUserRole> : DbContext
        where TUserRole : IdentityUserRole<TKey>
    { 
        /// <summary>
        /// Constructor that takes a DbManager instance 
        /// </summary>
        public RoleDbContext(string connectionString) : base(connectionString)
        {}

        /// <summary>
        /// Deltes a role from the Roles table
        /// </summary>
        /// <param name="roleId">The role Id</param>
        /// <returns></returns>
        public void Delete(TKey roleId)
        {
            Connection.Execute(@"Delete from Role where Id = @id", new { id = roleId });
        }

        /// <summary>
        /// Inserts a new Role in the Roles table
        /// </summary>
        /// <param name="roleName">The role's name</param>
        /// <returns></returns>
        public void Insert(IdentityRole<TKey, TUserRole> role)
        {
            Connection.Execute(@"Insert into Role (Name) values (@name)",
                new { name = role.Name });
        }

        /// <summary>
        /// Returns a role name given the roleId
        /// </summary>
        /// <param name="roleId">The role Id</param>
        /// <returns>Role name</returns>
        public string GetRoleName(TKey roleId)
        {
            return Connection.ExecuteScalar<string>("Select Name from Role where Id=@id", new { id = roleId });
        }

        /// <summary>
        /// Returns the role Id given a role name
        /// </summary>
        /// <param name="roleName">Role's name</param>
        /// <returns>Role's Id</returns>
        public TKey GetRoleId(string roleName)
        {
            return Connection.ExecuteScalar<TKey>("Select Id from Role where Name=@name", new { name = roleName });
        }

        /// <summary>
        /// Gets the IdentityRole given the role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public IdentityRole<TKey, TUserRole> GetRoleById(TKey roleId)
        {
            var roleName = GetRoleName(roleId);
            IdentityRole<TKey, TUserRole> role = null;

            if (roleName != null)
            {
                role = new IdentityRole<TKey, TUserRole>(roleName, roleId);
            }

            return role;
        }

        /// <summary>
        /// Gets the IdentityRole given the role name
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public IdentityRole<TKey, TUserRole> GetRoleByName(string roleName)
        {
            var roleId = GetRoleId(roleName);
            IdentityRole<TKey, TUserRole> role = null;

            //if (roleId > 0)
            //{
                role = new IdentityRole<TKey, TUserRole>(roleName, roleId);
            //}

            return role;
        }

        public void Update(IdentityRole<TKey, TUserRole> role)
        {
            Connection
            .Execute(@"
                    UPDATE Role
                    SET
                        Name = @name
                    WHERE
                        Id = @id",
                    new
                    {
                        name = role.Name,
                        id = role.Id
                    });
        }


    }
}
